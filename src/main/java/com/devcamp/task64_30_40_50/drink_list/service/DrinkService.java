package com.devcamp.task64_30_40_50.drink_list.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.devcamp.task64_30_40_50.drink_list.model.CDrink;
import com.devcamp.task64_30_40_50.drink_list.repository.JDrinkRepository;

@Service
public class DrinkService {
 
    @Autowired
    JDrinkRepository drinkRepository;

    public ArrayList<CDrink> getDrinkList(){
        ArrayList<CDrink> listDrink = new ArrayList<>();
        drinkRepository.findAll().forEach(listDrink::add);
        return listDrink;
    } 

    public Optional<CDrink> getDrinkByIdd(int id){
        return drinkRepository.findById(id);
    }
}
