package com.devcamp.task64_30_40_50.drink_list.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task64_30_40_50.drink_list.model.CDrink;

public interface JDrinkRepository extends JpaRepository<CDrink, Integer> {
    
}
