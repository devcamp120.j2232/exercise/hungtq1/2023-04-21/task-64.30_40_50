package com.devcamp.task64_30_40_50.drink_list.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task64_30_40_50.drink_list.model.CDrink;
import com.devcamp.task64_30_40_50.drink_list.repository.JDrinkRepository;
import com.devcamp.task64_30_40_50.drink_list.service.DrinkService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class DrinkController {
    
    @Autowired
    JDrinkRepository drinkRepository;
    @Autowired
    DrinkService drinkService;

    @GetMapping("/all-drinks")
    public ResponseEntity <List<CDrink>> getAllDrinks(){
        try {
            return new ResponseEntity<>(drinkService.getDrinkList(),HttpStatus.OK);            
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);   
        }
    }

    //applied service
    @GetMapping("/drink/{id}")
    public ResponseEntity <CDrink> getDrinkByIdService(@PathVariable("id") Integer id){
        Optional<CDrink> target = drinkService.getDrinkByIdd(id);
            if (target.isPresent()){
                return new ResponseEntity<>(target.get(), HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }          
    }

    //not applied service
    @GetMapping("/drinks/{id}")
    public ResponseEntity <CDrink> getDrinkById(@PathVariable("id") int id){
        Optional<CDrink> drink = drinkRepository.findById(id);
        if (drink.isPresent()){
            return new ResponseEntity<>(drink.get(), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/drink")
    public ResponseEntity<Object> createDrink(@RequestBody CDrink pDrink){
        try {
            Optional<CDrink> drinkData = drinkRepository.findById(pDrink.getId());
            if (drinkData.isPresent()){
                return ResponseEntity.unprocessableEntity().body("The drink already exists");
            }
            pDrink.setNgayTao(new Date());
            pDrink.setNgayCapNhat(null);
            CDrink _drink = drinkRepository.save(pDrink);
            return new ResponseEntity<>(_drink, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("Error: "+e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity().body("Fail to create specified drink" + e.getCause().getCause().getMessage());
        }
     }
}
